<?php
namespace cudn\Phalcon\Translate;

use Phalcon\Mvc\User\Plugin;
use Phalcon\Translate\Adapter\NativeArray;

/**
 * Class Translate
 * Translate language
 *
 * @package Bcore\Helper
 */
class Translate extends Plugin
{
	protected $languageDir = [];

	protected $defaultLanguage;

	protected $currentLanguage;

	/**
	 * Translate constructor.
	 *
	 * @param string $_languageDir
	 * @param string $_defaultLanguage
	 */
	public function __construct($_languageDir = '/messages/', $_defaultLanguage = 'en')
	{
		$this->languageDir[]   = $_languageDir;
		$this->defaultLanguage = $_defaultLanguage;
	}

	/**
	 * register a directory language
	 *
	 * @param string $dir
	 */
	public function registerTranslationDir($dir)
	{
		$this->languageDir[] = $dir;
	}

	/**
	 * set default language
	 *
	 * @param string $lang
	 */
	public function setDefaultLanguage($lang)
	{
		$this->defaultLanguage = $lang;
	}

	/**
	 * get default language
	 *
	 * @return string
	 */
	public function getDefaultLanguage()
	{
		return $this->defaultLanguage;
	}

	/**
	 * set entered language
	 *
	 * @param string $lang
	 */
	public function setCurrentLanguage($lang)
	{
		$this->currentLanguage = $lang;

	}


	/**
	 * get entered language
	 *
	 * @return string
	 */
	public function getCurrentLanguage()
	{
		return (!empty($this->currentLanguage)) ? $this->currentLanguage : $this->defaultLanguage;
	}


	/*
	 * get translate language
	 */
	public function getTranslation()
	{

		// Get entered language.
		//$language = $this->dispatcher->getParam('lang');
		$language = !empty($this->currentLanguage) ? $this->currentLanguage : $this->defaultLanguage;


		// Ask browser what is the best language

		if (empty($language)) {
			$language = $this->request->getBestLanguage();

		}

		$messages = [];

		foreach ($this->languageDir as $dir) {

			if (is_readable($dir . $language . '.php')) {
				$override = include_once $dir . $language . '.php';
				if (is_array($override)) {
					$messages = array_merge($messages, $override);
				}
			}
		}

		if (empty($messages)) {
			foreach ($this->languageDir as $dir) {
				if (is_readable($dir . $this->defaultLanguage . '.php')) {
					$override = include_once $dir . $this->defaultLanguage . '.php';
					if (is_array($override)) {
						$messages = array_merge($messages, $override);
					}
				}
			}
		}


		// Return a translation object
		return new NativeArray(
			[
				"content" => $messages,
			]
		);
	}
}